# Home Assistant: Github release monitor

This Home Assistant component adds support for monitoring tag and release publications on Github repositories.

The component will broadcast a Home Assistant event when a new version is detected.

## Installation instructions

* Download one of the releases of this component
* Upload the extracted release to the `config` home of your home assistant installation

## Configuration example
```yaml
sensor:
  - platform: github_release
    tag:
      - project: my-project-key
        repo: my-repo-in-the-project
    release:
      - project: my-project-key
        repo: my-repo-in-the-project
```

You are able to configure on or more repositories you want to monitor. And have the option to monitor release and or
tags for that repository by adding the project / repository under either `tag` or `release` in the configuration.

## Automation example

```yaml
alias: Send notification of Github Updates
trigger:
  platform: event
  event_type: github_release
action:
  - service: persistent_notification.create
    data_template:
      title: "A new {{ trigger.event.data.repository }} release available"
      message: "A new version of {{ trigger.event.data.repository }} was released with version {{ trigger.event.data.event.title }}"
      notification_id: "{{ trigger.event.data.event.title }}"
```

In the event fired the following variables are available:

Variable | Description
--- | ---
trigger.event.data.project | The name of the Github project
trigger.event.data.repository | The name of the Github repository
trigger.event.data.event.link | The link to the tag or release
trigger.event.data.event.updated | The date the release or tag was created
trigger.event.data.event.title | The name of the tag or release
trigger.event.data.event.author | The contributor that created the tag or release 

### Group example
```yaml
name: Monitored Github
entities:
  -  sensor.github_tags_dresden_elektronik_deconz_rest_plugin
```
