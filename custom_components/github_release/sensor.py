"""
@ Author      : Gerben Jongerius
@ Date        : 01/18/2018
@ Description : Github Release Sensor
Configuration sample

sensor:
  - platform: github_release
    releases:
      - project: my-sample-project
        repo: repo-in-project
"""
import datetime
import logging
import voluptuous as vol

import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle

_LOGGER = logging.getLogger(__name__)

DOMAIN = 'github'
SENSOR_PREFIX = 'sensor.github_'
CONF_RELEASES = "release"
CONF_TAGS = "tag"
EVENT_GITHUB_MONITOR = "github_release"

REQUIREMENTS = ['feedparser==5.2.1']

CONFIG_SCHEMA = vol.Schema({
    DOMAIN: vol.Schema({
        vol.Optional(CONF_RELEASES): vol.All(cv.ensure_list, [cv.has_at_least_one_key]),
        vol.Optional(CONF_TAGS): vol.All(cv.ensure_list, [cv.has_at_least_one_key])
    })
}, extra=vol.ALLOW_EXTRA)


def setup_platform(hass, config, add_devices, discovery_info=None):
    mon_releases = config.get(CONF_RELEASES) or None
    mon_tags = config.get(CONF_TAGS) or None

    _LOGGER.debug("Setting up the Github Release monitor")
    if mon_releases is not None:
        add_devices([FeedReader(module, 'releases', hass) for module in mon_releases])
    if mon_tags is not None:
        add_devices([FeedReader(module, 'tags', hass) for module in mon_tags])


class FeedReader(Entity):

    def __init__(self, package, type, hass):
        _LOGGER.info("Setting up sensor for github project " + package['project'] + " and repo " + package['repo'])
        self._project = package['project']
        self._repo = package['repo']
        self._uri = 'https://github.com/' + package['project'] + '/' + package['repo'] + '/' + type + '.atom'
        self._state = None
        self._feed = None
        self._entry = None
        self._hass = hass
        self.entity_id = SENSOR_PREFIX + type + '_' + package['project'].replace('-', '_') + '_' + package['repo'].replace('-', '_')

    @property
    def name(self):
        return self._project + ": " + self._repo

    @property
    def state(self):
        return self._state

    @property
    def entity_picture(self):
        return '/local/github/logo.png'

    @property
    def state_attributes(self):
        if self._entry is not None:
            return {
                'link': self._entry.link,
                'updated': self._entry.updated,
                'title': self._entry.title,
                'author': self._entry.author_detail.name
            }
        return {}

    @Throttle(datetime.timedelta(hours=1))
    def update(self):
        import feedparser
        _LOGGER.info("Parsing Github feed data from " + self._uri)
        self._feed = feedparser.parse(self._uri,
                                      etag=None if not self._feed
                                      else self._feed.get('etag'),
                                      modified=None if not self._feed
                                      else self._feed.get('modified'))

        if not self._feed:
            self._state = 'Fetch error'
        else:
            if self._feed.bozo != 0:
                self._state = 'Parse error'
            elif self._feed.entries:
                old_state = self._state
                self._entry = self._feed.entries[0]
                id_split = self._entry.id.split("/")
                self._state = id_split[len(id_split) - 1]
                if old_state is not None and old_state != self._state:
                    _LOGGER.info("Firing event for new publication of github release for " + self._repo)
                    self._hass.bus.fire(EVENT_GITHUB_MONITOR, {
                        'project': self._project,
                        'repository': self._repo,
                        'event': {
                            'link': self._entry.link,
                            'updated': self._entry.updated,
                            'title': self._entry.title,
                            'author': self._entry.author_detail.name
                        }
                    })
